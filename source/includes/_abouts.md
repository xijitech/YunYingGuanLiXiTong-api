# 关于我们

## 关于我们

```shell
curl "http://amblem.xijitech.com/api/v1/abouts"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/abouts`

### Query Parameters


Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "about": {
        "id": 1,
        "about_us": "<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>5000年前，中国￭杭州￭余杭，勤劳智慧的良渚人在这里开启了中国厨房文明<br />1979年，良渚后人老板电器在这里开启了中国现代厨房文明。<br />38年专注高端，老板推出智能大吸力油烟机，在今天，中国平均每一分钟就有家庭选择老板吸油烟机，老板，大吸力油烟机。1979年成立，38年专注高端厨房电器，高端油烟机；连续十年获亚洲品牌500强。老板，精湛科技，轻松烹饪，创造人类对厨房生活的一切美好向往。<br />2006年，老板电器厨房延伸项目&mdash;&mdash;安泊整体厨房上市<br />2006年，安泊100亩智造基地落成，全系德国生产线，298道精工厨柜制作工序，年产100000套能力，刷新行业精造之道<br />10年来，安泊厨柜获得中国《家用厨房设备》国家标准起草单位，中国五金制品协会厨房设备分会副会长单位，浙商名家具金奖企业等20多项行业殊荣<br />截至2015年，安泊凭借&ldquo;360&ordm;全方位防潮技术&rdquo;、&ldquo;活性无醛技术&rdquo;、&ldquo;固特易（Easy＆durable）&rdquo;组装工艺等先进技术在健康、环保、耐用硬实力领域共获得10余项专利，持续保持行业领先地位<br />2016年，安泊开创功能厨房时代，以诚实的态度，深入生活的角度、以及领先的厨房功能平台和充满生活巧思的设计用心打造中国人的&rdquo;一厨一生活&ldquo;<br />2016年，安泊签约著名影星刘涛代言，并提出新的品牌主张&mdash;&mdash;&ldquo;安泊，一厨一生活&rdquo;，由此展开新一轮品牌形象构建<br />2016年，安泊发布&ldquo;4大主题8种生活&rdquo;整体厨房；同期，安泊橱柜美学体验馆开馆</p>\r\n</body>\r\n</html>",
        "created_at": "2017-11-14T13:49:54.755+08:00",
        "updated_at": "2017-11-14T13:49:54.755+08:00"
    }
}
```
