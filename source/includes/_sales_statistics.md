# 销售统计

## 销售统计主页(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/tasks/sales_statistics?store=<SID>&start_date=DATE&end_date=DATE"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/tasks/sales_statistics?store=<SID>&start_date=DATE&end_date=DATE`

### Query Parameters

Parameter | Description
--------- | -----------
SID|门店ID
start_date|时间范围筛选，开始时间，格式为 2018-01-03
end_date|时间范围筛选，结束时间，格式为 2018-01-03

> 返回内容

```json
{
    "money": "20000.0",
    "stores": [
        {
            "id": 7,
            "name": "南桥贤马店"
        }
    ],
    "tasks": [
        {
            "id": 9,
            "name": "店长",
            "task_user": "15099999999",
            "task_user_telephone": "15099999999",
            "serial_num": "20175320900565",
            "store_name": "宝山建配龙店",
            "customer_name": "店长",
            "customer_tel": "18876549876",
            "customer_gender": "男",
            "province_name": "上海",
            "city_name": "上海",
            "area_name": "黄浦区",
            "customer_address": "店长",
            "market_price": null,
            "contract_price": null,
            "signed_at": null
        }
    ]
}
```
