# 报价清单

## 报价清单列表(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/offering_lists?task_id=<TID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/offering_lists?task_id=<TID>`

### Query Parameters


Parameter | Required | Description
--------- | -------- | -----------
TID|true|任务ID


> 返回内容

```json
{
    "offering_lists": [
        {
            "id": 31,
            "serial_num": "20173614856743",
            "count": "1",
            "sum": "9180.00"
        }
    ]
}
```


## 报价清单详情页面

```shell
curl "http://amblem.xijitech.com/api/v1/offering_lists/<OLID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/offering_lists/<OLID>`

### Query Parameters

Parameter | Description
--------- | -----------
OLID|报价清单ID

> 返回内容

```json
{
    "task_id": 1,
    "offering_list": {
        "id": 2,
        "serial_num": "20174910264068",
        "task_customer_name": "客户姓名",
        "task_customer_tel": "15000000000",
        "task_customer_address": "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊",
        "count": "1",
        "sum": "120.00",
        "carts": [
            {
                "id": 7,
                "merchandise_name": "标准地柜",
                "merchandise_cover": "/uploads/cart/merchandise_cover/7/thumb_haixinghezhenzhubeikeshiliangbeijing_3937174.jpg",
                "merchandise_sku": "单双面：单面， 表面处理：高光， 颜色：MK7101天山雪莲， 门型：A01 B01",
                "merchandise_price": "20.00",
                "merchandise_default_unit": "米",
                "merchandise_quantity": "6",
                "merchandise_quantity_merchandise_price": 120,
                "created_at": "2017-11-09T15:13:13.947+08:00"
            }
        ]
    }
}
```


## 报价清单编辑页面

```shell
curl "http://amblem.xijitech.com/api/v1/offering_lists/<OLID>/edit"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/offering_lists/<OLID>/edit`

### Query Parameters

Parameter | Description
--------- | -----------
OLID|报价清单ID

> 返回内容

```json
{
    "offering_list": {
        "id": 31,
        "serial_num": "20173614856743",
        "task_customer_name": "张飞",
        "task_customer_tel": "15000000000",
        "task_customer_address": "张飞路2000号",
        "count": "1",
        "sum": "9180.00",
        "carts": [
            {
                "id": 112,
                "merchandise_name": "标准地柜",
                "merchandise_cover": "/uploads/cart/merchandise_cover/112/thumb_%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20171020174108.png",
                "merchandise_sku": "高度尺寸：700， 产品属性：标准， 柜体颜色：白橡木纹浮雕多层板， 深度尺寸：574， 门板颜色：MT3101鲁冰花",
                "merchandise_price": "3060.00",
                "merchandise_default_unit": "延米",
                "merchandise_quantity": "3",
                "merchandise_quantity_merchandise_price": 9180,
                "created_at": "2017-11-08T10:18:52.531+08:00"
            }
        ]
    }
}
```



## 生成报价和更新报价

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/offering_lists"
```
### HTTP Request

    `POST http://amblem.xijitech.com/api/v1/offering_lists`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
offering_lists_id|0,1,2,3|true|新建为0，更新为该OfferingListsId
task_id| |true|任务ID
line_item_ids|[1,2,3,4]|true|预算清单ID
line_item_values| |true|预算清单商品的个数，下标与line_item_ids对应

> 返回内容

```json
{
    "status": "success",
    "offering_list": 3
}
```



## 删除报价清单

```shell
curl -X DELETE "http://amblem.xijitech.com/api/v1/offering_lists/<OID>"
```
### HTTP Request

    `DELETE http://amblem.xijitech.com/api/v1/offering_lists/<OID>`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
OID| |true|报价清单ID

> 返回内容

```json
{
    "status": "success"
}
```
