# 地址

## 获得城市

```shell
curl "http://amblem.xijitech.com/api/v1/get_cities?province_id=<PID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/get_cities?province_id=<PID>`

### Query Parameters

Parameter | Required | Description
--------- | ------- | ---------
PID|true|省市ID

> 返回内容

```json
[
    {
        "id": 25,
        "province_id": 5,
        "name": "呼和浩特",
        "created_at": "2017-09-28T10:28:08.896+08:00",
        "updated_at": "2017-09-28T10:28:08.896+08:00"
    },
    {
        "id": 26,
        "province_id": 5,
        "name": "包头",
        "created_at": "2017-09-28T10:28:08.907+08:00",
        "updated_at": "2017-09-28T10:28:08.907+08:00"
    }
]
```


## 获得县区

```shell
curl "http://amblem.xijitech.com/api/v1/get_areas?city_id=<CID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/get_areas?city_id=<CID>`

### Query Parameters

Parameter | Required | Description
--------- | ------- | ---------
CID|true|城市ID

> 返回内容

```json
[
    {
        "id": 274,
        "city_id": 21,
        "name": "离石区",
        "created_at": "2017-09-28T10:28:08.830+08:00",
        "updated_at": "2017-09-28T10:28:08.830+08:00"
    },
    {
        "id": 275,
        "city_id": 21,
        "name": "孝义市",
        "created_at": "2017-09-28T10:28:08.831+08:00",
        "updated_at": "2017-09-28T10:28:08.831+08:00"
    }
]
```
