# 任务

## 任务主页(分页page,搜索)

```shell
curl "http://amblem.xijitech.com/api/v1/tasks?store=<SID>status=STATUS&q=Q&start_date=DATE&end_date=DATE"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/tasks?store=<SID>&status=STATUS&q=Q&start_date=DATE&end_date=DATE`

### Query Parameters

Parameter | Required | Description
--------- | -------- | -----------
SID|false|店面ID
status|false|任务状态，比如wait_offer
start_date|false|时间范围筛选，开始时间，格式为 2018-01-03
end_date|false|时间范围筛选，结束时间，格式为 2018-01-03
Q|false|搜索内容

> 返回内容

```json
{
    "states": [
        {
            "id": "wait_offer",
            "name": "待报单"
        },
        {
            "id": "wait_assign",
            "name": "待分配"
        },
        {
            "id": "wait_appoint",
            "name": "待预约"
        },
        {
            "id": "wait_first_test",
            "name": "待初测"
        },
        ...
    ],
    "stores": [
        {
            "id": 7,
            "name": "南桥贤马店"
        }
    ],
    "tasks": [
        {
            "id": 8,
            "name": "销售",
            "status": "已取消",
            "store_name": "宝山建配龙店",
            "customer_name": "销售",
            "customer_tel": "18876549876",
            "customer_address": "销售",
            "task_create_user": "销售",
            "task_create_user_telephone": "15011112222",
            "designer": "待分配",
            "designer_telephone": "待分配",
            "next_task": "待预约",
            "reject_reason": null
        }
    ]
}
```


## 任务详情页面

```shell
curl "http://amblem.xijitech.com/api/v1/tasks/<TID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/tasks/<TID>`

### Query Parameters

Parameter | Description
--------- | -----------
TID|任务ID

> 返回内容

```json
{
    "task": {
        "id": 7,
        "name": "第三方渠道",
        "serial_num": "20175620476128",
        "store_name": "宝山建配龙店",
        "customer_name": "张三",
        "customer_tel": "18766665555",
        "customer_gender": "女",
        "province_name": "上海",
        "city_name": "上海",
        "area_name": "卢湾区",
        "customer_address": "第三方渠道",
        "remarks": "",
        "market_price": null,
        "contract_price": null,
        "signed_at": null,
        "finished_at": "2017-10-20",
        "designer": "设计师",
        "next_task": "待预约"
    }
}
```

## 编辑任务页面

```shell
curl "http://amblem.xijitech.com/api/v1/tasks/<TID>/edit"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/tasks/<TID>/edit`

### Query Parameters

Parameter | Description
--------- | -----------
TID|任务ID

> 返回内容

```json
{
    "task": {
        "id": 10,
        "name": "老板",
        "serial_num": "20175720249726",
        "store_name": "宝山建配龙店",
        "customer_name": "老板",
        "customer_tel": "18876549876",
        "customer_gender": "男",
        "province_name": "上海",
        "city_name": "上海",
        "area_name": "黄浦区",
        "customer_address": "老板",
        "remarks": "",
        "market_price": null,
        "contract_price": null,
        "signed_at": null,
        "finished_at": null,
        "designer": "待分配"
    },
    "customer_gender": {
        "1": "男",
        "2": "女"
    },
    "designer": {
        "9": "设计主管",
        "10": "设计师",
        "29": "Arss杨",
        "32": "陈宇"
    },
    "province": [
        {
            "id": 9,
            "name": "上海"
        }
    ],
    "cities": [
        {
            "id": 76,
            "name": "上海"
        }
    ],
    "areas": [
        {
            "id": 776,
            "name": "其他"
        }
    ]
}
```


## 新建任务页面

```shell
curl "http://amblem.xijitech.com/api/v1/tasks/new"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/tasks/new`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "customer_gender": {
        "1": "男",
        "2": "女"
    },
    "stores": [
        {
            "id": 4,
            "name": "上海三店"
        }
    ],
    "tasks": {
        "id": null,
        "serial_num": null,
        "store_id": null,
        "customer_name": null,
        "customer_tel": null,
        "customer_gender": null,
        "customer_address": null,
        "province_id": null,
        "city_id": null,
        "area_id": null,
        "remarks": null,
        "status": "wait_offer",
        "user_id": null,
        "designer_id": null,
        "operation_center_id": null,
        "created_at": null,
        "updated_at": null,
        "name": null,
        "market_price": null,
        "contract_price": null,
        "signed_at": null,
        "finished_at": null,
        "reject_reason": null,
        "reject_task": null
    },
    "province": [
        {
            "id": 9,
            "name": "上海"
        },
        {
            "id": 25,
            "name": "云南"
        }
    ]
}
```


## 创建任务

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/tasks"
```
### HTTP Request

    `POST http://amblem.xijitech.com/api/v1/tasks`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
task[store_id]|1|true|门店ID
task[name]|任务测试-名称|true|任务名称
task[customer_name]|李先生|true|客户名称
task[customer_tel]|15100000000|true|客户联系号码
task[customer_gender]|1,2|true|客户性别
task[province_id]|1|true|省市ID
task[city_id]| |true|城市ID
task[area_id]| |true|地区ID
task[customer_address]|同济大学|true|客户详细地址
task[remarks]|客户很急，设计师请尽快与客户联系|false|备注

> 返回内容

```json
{
    "status": "success",
    "task_id": 13
}
```

## 删除任务(未用到)

```shell
curl -X DELETE "http://amblem.xijitech.com/api/v1/tasks/<TID>"
```
### HTTP Request

    `DELETE http://amblem.xijitech.com/api/v1/tasks/<TID>`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
TID|13|true|任务ID

> 返回内容

```json
{
    "status": "success"
}
```

## 更新任务

```shell
curl -X PUT "http://amblem.xijitech.com/api/v1/tasks/<TID>"
```
### HTTP Request

    `PUT http://amblem.xijitech.com/api/v1/tasks/<TID>`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
task[store_id]|1|true|门店ID
task[name]|任务测试-名称|true|任务名称
task[customer_name]|李先生|true|客户名称
task[customer_tel]|15100000000|true|客户联系号码
task[customer_gender]|1,2|true|客户性别
task[province_id]|1|true|省市ID
task[city_id]| |true|城市ID
task[area_id]| |true|地区ID
task[customer_address]|同济大学|true|客户详细地址
task[remarks]|客户很急，设计师请尽快与客户联系|false|备注
task[market_price]|2000|false|市场价
task[contract_price]|2000|false|合同金额
task[signed_at]|2017-11-22|false|合同签订时间
task[finished_at]|2017-11-22|false|预估完成时间
task[designer_id]| |false|设计师ID

> 返回内容

```json
{
    "status": "success",
    "task_id": 12
}
```

## 取消任务

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/tasks/<TID>/canceled"
```
### HTTP Request

    `POST http://amblem.xijitech.com/api/v1/tasks/<TID>/canceled`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
TID|12|true|任务ID

> 返回内容

```json
{
    "status": "success"
}
```



## 任务图纸管理(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/tasks/<TID>/task_pics"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/tasks/<TID>/task_pics`

### Query Parameters

Parameter | Description
--------- | -----------
TID|任务ID

> 返回内容

```json
{
    "task_pics": [
        {
            "id": 4,
            "name": null,
            "task_id": 2,
            "pic": "/uploads/task_pic/pic/4/%23%3CActionDispatch%3A%3AHttp%3A%3AUploadedFile%3A0x00007fa3794cec98%3E"
        },
        {
            "id": 5,
            "name": null,
            "task_id": 2,
            "pic": "/uploads/task_pic/pic/5/83B42A46FBB8210761603CE7BE77AC4C.jpg"
            "pic_t": "/uploads/task_pic/pic/5/thumb_83B42A46FBB8210761603CE7BE77AC4C.jpg"
        }
    ],
    "task_pic": {
        "id": null,
        "name": null,
        "task_id": null,
        "pic": null
        "pic_t": null
    }
}
```


## 上传图片

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/tasks/<TID>/task_pics"
```
### HTTP Request

    `POST http://amblem.xijitech.com/api/v1/tasks/<TID>/task_pics`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
TID|12|true|任务ID

task_pic[pic]| |true|图片

> 返回内容

```json
{
    "status": "success",
    "order_id": 2
}
```


## 删除任务图纸

```shell
curl -X DELETE "http://amblem.xijitech.com/api/v1/tasks/<TID>/task_pics/<PID>"
```
### HTTP Request

    `DELETE http://amblem.xijitech.com/api/v1/tasks/<TID>/task_pics/<PID>`

### Query Parameters

Parameter | Description
--------- | -----------
TID|任务ID
PID|图纸ID

> 返回内容

```json
{
    "status": "success",
    "order_id": 2
}
```


## 任务进行下一步

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/tasks/<TID>/operate_event"
```

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/tasks/<TID>/operate_event`


### FormData

Parameter | Required | Description
--------- | ------- | ---------
TID|true|任务ID


> 返回内容

```json
{
  "status":"success"
}
```



## 任务退回上一步

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/tasks/<TID>/reject_event" -d "reject_reason=REASON"
```

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/tasks/<TID>/reject_event`


### FormData

Parameter | Required | Description
--------- | ------- | ---------
TID|true|任务ID
reject_reason|true|退回理由


> 返回内容

```json
{
  "status":"success"
}
```

## 任务订单

```shell
curl "http://amblem.xijitech.com/api/v1/tasks/<ID>/orders"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/tasks/<ID>/orders`

### Query Parameters


Parameter | Required | Description
--------- | -------- | -----------
ID|true|任务ID

> 返回内容

```json
{
    "task_id": 1,
    "orders": [
        {
            "id": 1,
            "status": "已完成",
            "order_type": null,
            "store_name": "上海三店",
            "serial_num": "20170631155287",
            "serial_num_user": "123123123",
            "express_num": "123123123",
            "task_customer_name": "客户姓名",
            "task_customer_tel": "15000000000",
            "task_customer_address": "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊",
            "task_user_nick": "工号9527，",
            "task_user_telephone": "15000000000",
            "designer_nick": "工号9527，",
            "designer_telephone": "15000000000",
            "installer_nick": "安装师9527",
            "installer_telephone": "18621335061",
            "reject_reason": null
        },
        {
            "id": 3,
            "status": "待分配",
            "order_type": null,
            "store_name": "上海三店",
            "serial_num": "20175110576059",
            "serial_num_user": "",
            "express_num": "",
            "task_customer_name": "客户姓名",
            "task_customer_tel": "15000000000",
            "task_customer_address": "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊",
            "task_user_nick": "工号9527，",
            "task_user_telephone": "15000000000",
            "designer_nick": "工号9527，",
            "designer_telephone": "15000000000",
            "installer_nick": "待分配",
            "installer_telephone": "待分配",
            "reject_reason": null
        },
        {
            "id": 2,
            "status": "待审核",
            "order_type": "台面",
            "store_name": "上海三店",
            "serial_num": "20175431747564",
            "serial_num_user": "11111111111",
            "express_num": "111111111111",
            "task_customer_name": "客户姓名",
            "task_customer_tel": "15000000000",
            "task_customer_address": "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊",
            "task_user_nick": "工号9527，",
            "task_user_telephone": "15000000000",
            "designer_nick": "工号9527，",
            "designer_telephone": "15000000000",
            "installer_nick": "安装师9527",
            "installer_telephone": "18621335061",
            "reject_reason": null
        }
    ]
}
```
