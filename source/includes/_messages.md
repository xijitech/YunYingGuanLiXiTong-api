# 消息

## 未读消息(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/messages"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/messages`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "messages": [
        {
            "id": 41,
            "name": "20173114771320",
            "message_type": "订单",
            "see": false,
            "msg": "您的订单被修改，请前往查看！",
            "record_id": 3,
            "created_at": "2017-11-15 22:34"
        }
    ]
}
```

## 全部消息(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/messages/all_message"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/messages/all_message`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "messages": [
        {
            "id": 41,
            "name": "20173114771320",
            "message_type": "订单",
            "see": false,
            "msg": "您的订单被修改，请前往查看！",
            "record_id": 3,
            "created_at": "2017-11-15 22:34"
        }
    ]
}
```

## 未读消息数量

```shell
curl "http://amblem.xijitech.com/api/v1/messages/messages_count"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/messages/messages_count`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "messages": 58
}
```


## 更改消息状态消息

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/messages/<MID>/read_message"
```
### HTTP Request

    `POST http://amblem.xijitech.com/api/v1/messages/<MID>/read_message`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
MID| |true|消息ID

> 返回内容

```json
{
    "status": "success"
}
```
