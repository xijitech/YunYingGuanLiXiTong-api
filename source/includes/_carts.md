# 预算

## 预算清单

```shell
curl "http://amblem.xijitech.com/api/v1/get_my_cart"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/get_my_cart`

### Query Parameters


Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "tasks": [
        {
            "id": 11,
            "name": "测试-买家具"
        }
    ],
    "carts": [
        {
            "id": 116,
            "merchandise_id": 8,
            "merchandise_sku_id": 45088,
            "merchandise_quantity": "20",
            "created_at": "2017-11-14T15:00:22.303+08:00"
        },
        {
            "id": 117,
            "merchandise_id": 6,
            "merchandise_sku_id": 44777,
            "merchandise_quantity": "1",
            "created_at": "2017-11-14T15:01:08.907+08:00"
        }
    ],
    "merchandises": [
        {
            "id": 8,
            "name": "标准中柜",
            "desc": "整体板A",
            "cover": "/uploads/covered_obj/cover/237/thumb_%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20171020174120.png",
            "created_at": "2017-10-06T14:26:03.777+08:00"
        },
        {
            "id": 6,
            "name": "标准地柜",
            "desc": "整体板A",
            "cover": "/uploads/covered_obj/cover/235/thumb_%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20171020174108.png",
            "created_at": "2017-10-05T11:17:55.492+08:00"
        }
    ],
    "merchandises_skus": [
        {
            "id": 44777,
            "merchandise_id": 6,
            "price": "2448.00",
            "cost": "1.00",
            "stock": "999",
            "sku": {
                "高度尺寸": "700",
                "产品属性": "标准",
                "柜体颜色": "白橡木纹浮雕多层板",
                "深度尺寸": "350",
                "门板颜色": "MT3114金缕沙丘"
            },
            "created_at": "2017-10-20T17:41:07.977+08:00"
        },
        {
            "id": 45088,
            "merchandise_id": 8,
            "price": "3830.00",
            "cost": "1.00",
            "stock": "999",
            "sku": {
                "高度尺寸": "1360",
                "产品属性": "标准",
                "深度尺寸": "420",
                "柜体颜色": "奥运橡多层板",
                "门板颜色": "MT3104黄山秋晓"
            },
            "created_at": "2017-10-20T17:42:00.734+08:00"
        }
    ]
}
```

## 商品加入预算清单

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/gen_line_items"
```
### HTTP Request

    `POST http://amblem.xijitech.com/api/v1/gen_line_items`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
merchandise_id|5|true|商品ID
property_id|12|false|商品型号ID
default_unit|50|false|个数（如 ‘50’ 米）

> 返回内容

```json
{
    "status": "success"
}
```


## 删除加入预算的商品

```shell
curl -X DELETE "http://amblem.xijitech.com/api/v1/carts/<CID>"
```
### HTTP Request

    `DELETE http://amblem.xijitech.com/api/v1/carts/<CID>`

### Query Parameters

Parameter | Description
--------- | -----------
CID|商品ID

> 返回内容

```json
{
    "status": "success"
}
```
