# 佣金结算

## 佣金结算(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/commissions?status=STATUS&store=STORE&start_date=DATE&end_date=DATE"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/commissions?status=STATUS&store=STORE&start_date=DATE&end_date=DATE`

### Query Parameters

Parameter | Description
--------- | -----------
status|佣金状态（可不填）
store|店面ID（可不填）
start_date|时间范围筛选，开始时间，格式为 2018-01-03
end_date|时间范围筛选，结束时间，格式为 2018-01-03

> 返回内容

```json
{
    "status": [
        {
            "id": "wait_grant",
            "name": "待发放"
        },
        {
            "id": "wait_receive",
            "name": "待领取"
        },
        {
            "id": "settled",
            "name": "已结算"
        },
        {
            "id": "canceled",
            "name": "已取消"
        }
      ],
    "money": 2000,
    "stores": [
        {
            "id": 4,
            "name": "上海三店"
        }
    ],
    "commissions": [
        {
            "id": 1,
            "user_id": "安装师9527",
            "commission": "0.00",
            "store": "上海三店",
            "status": "待发放",
            "next_commission": "待领取",
            "record_id": 1,
            "record_name": "任务",
            "record": {
                "task_name": "测试名称",
                "task_customer_name": "客户姓名",
                "task_customer_tel": "15000000000",
                "task_customer_address": "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊"
            }
        },
        {
            "id": 3,
            "user_id": "安装师9527",
            "commission": "0.00",
            "store": "上海三店",
            "status": "待发放",
            "next_commission": "待领取",
            "record_id": 1,
            "record_name": "订单",
            "record": {
                "order_serial_num_user": "123123123",
                "order_express_num": "123123123",
                "order_task_customer_name": "客户姓名",
                "order_task_customer_tel": "15000000000",
                "order_task_customer_address": "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊"
            }
        },
        {
            "id": 2,
            "user_id": "安装师9527",
            "commission": "0.00",
            "store": "上海三店",
            "status": "已结算",
            "next_commission": null,
            "record_id": 1,
            "record_name": "订单",
            "record": {
                "order_serial_num_user": "123123123",
                "order_express_num": "123123123",
                "order_task_customer_name": "客户姓名",
                "order_task_customer_tel": "15000000000",
                "order_task_customer_address": "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊"
            }
        }
    ]
}
```


## 佣金进行下一步

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/commissions/<CID>/operate_event"
```

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/commissions/<CID>/operate_event`


### FormData

Parameter | Required | Description
--------- | ------- | ---------
CID|true|佣金ID


> 返回内容

```json
{
  "status":"success"
}
```
