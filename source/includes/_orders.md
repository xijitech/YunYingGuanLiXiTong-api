# 订单

## 订单主页(分页page,搜索)

```shell
curl "http://amblem.xijitech.com/api/v1/orders?store=<SID>status=STATUS&order_type=<OTID>&q=Q&start_date=DATE&end_date=DATE"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/orders?store=<SID>&status=STATUS&order_type=<OTID>&q=Q&start_date=DATE&end_date=DATE`

### Query Parameters


Parameter | Required | Description
--------- | -------- | -----------
SID|false|店面ID
STATUS|false|订单状态，比如wait_place
OTID|false|订单类型ID
start_date|false|时间范围筛选，开始时间，格式为 2018-01-03
end_date|false|时间范围筛选，结束时间，格式为 2018-01-03
Q|false|搜索内容

> 返回内容

```json
{
    "states": [
        {
            "id": "wait_offer",
            "name": "待报单"
        },
        {
            "id": "wait_assign",
            "name": "待分配"
        },
        {
            "id": "wait_appoint",
            "name": "待预约"
        },
        {
            "id": "wait_first_test",
            "name": "待初测"
        },
        ...
    ],
    "order_types": [
        {
            "id": 6,
            "name": "厨柜产品"
        },
        {
            "id": 8,
            "name": "电器产品"
        },
        {
            "id": 7,
            "name": "台面产品"
        },
        ...
    ],
    "stores": [
        {
            "id": 7,
            "name": "南桥贤马店"
        },
        ...
    ],
    "orders": [
        {
            "id": 3,
            "status": "待生产",
            "order_type": "台面产品",
            "store_name": "南桥贤马店",
            "serial_num": "20173114771320",
            "serial_num_user": "200111091728",
            "express_num": "622111222333",
            "task_customer_name": "张飞",
            "task_customer_tel": "15000000000",
            "task_customer_address": "张飞路2000号",
            "task_user_nick": "工号9527",
            "task_user_telephone": "15000000000",
            "designer_nick": "工号9527",
            "designer_telephone": "15000000000",
            "installer_nick": "安装师傅",
            "installer_telephone": "15077777777",
            "next_order": "待审核",
            "reject_reason": null
        },
        ...
    ]
}
```


## 订单详情页面

```shell
curl "http://amblem.xijitech.com/api/v1/orders/<OID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/orders/<OID>`

### Query Parameters

Parameter | Description
--------- | -----------
OID|订单ID

> 返回内容

```json
{
    "order": {
        "id": 2,
        "task_name": "测试名称",
        "serial_num": "20175431747564",
        "serial_num_user": "222",
        "task_store_name": "上海三店",
        "order_type_name": "台面",
        "installer_nick": "安装师9527",
        "express_num": "111111111111",
        "express_cost": "111.0",
        "price": "112.00",
        "design_price": "0.00",
        "fabricating_cost": "2240.00",
        "next_order": null,
        "remarks": ""
    },
    "properties": [
        {
            "name": "台面",
            "value": "112",
            "unit": "元"
        },
        {
            "name": "台面布",
            "value": "112",
            "unit": " 米"
        }
    ]
}
```



## 新建订单页面

```shell
curl "http://amblem.xijitech.com/api/v1/orders/new"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/orders/new`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "tasks": [
        {
            "id": 4,
            "name": "123123123"
        },
        {
            "id": 1,
            "name": "测试名称"
        }
    ],
    "order_types": [
        {
            "id": 2,
            "name": "台面"
        },
        {
            "id": 4,
            "name": "柜子"
        }
    ],
    "order": {
        "id": null,
        "task_id": null,
        "order_type_id": null,
        "serial_num_user": null,
        "express_num": null,
        "express_cost": null,
        "properties": {},
        "remarks": null
    }
}
```


## 编辑订单页面

```shell
curl "http://amblem.xijitech.com/api/v1/orders/<OID>/edit"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/orders/<OID>/edit`

### Query Parameters

Parameter | Description
--------- | -----------
OID|订单ID

> 返回内容

```json
{
    "fabricating_cost_false": [
        {
            "id": 20,
            "name": "台面部分",
            "default_unit": "元",
            "price": "0.00"
        }
    ],
    "fabricating_cost_true": [],
    "tasks": [
        {
            "id": 11,
            "name": "测试-买家具"
        }
    ],
    "order_types": [
        {
            "id": 6,
            "name": "厨柜产品"
        }
    ],
    "installers": [
        {
            "id": 12,
            "nick": "安装师傅"
        }
    ],
    "type": {
        "id": 7,
        "name": "台面产品"
    },
    "order": {
        "id": 3,
        "task_id": 11,
        "serial_num": "20173114771320",
        "serial_num_user": "200111091728",
        "task_store_name": "南桥贤马店",
        "order_type_id": 7,
        "installer_id": 12,
        "express_num": "622111222333",
        "express_cost": "300.0",
        "properties": {
            "台面部分": "2000",
            "台下盆": "20",
            "台面包管": "20",
            "收费边型": "20"
        },
        "remarks": ""
    }
}
```



## 订单图纸管理(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/orders/<OID>/order_pics"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/orders/<OID>/order_pics`

### Query Parameters

Parameter | Description
--------- | -----------
OID|订单ID

> 返回内容

```json
{
    "order_pics": [
        {
            "id": 2,
            "name": null,
            "order_id": 2,
            "pic": "/uploads/order_pic/pic/2/01E3CC67C67A9448463E15C0683CC0A0.jpg",
            "pic_t": "/uploads/order_pic/pic/2/thumb_01E3CC67C67A9448463E15C0683CC0A0.jpg"
        }
    ],
    "order_pic": {
        "id": null,
        "name": null,
        "order_id": null,
        "pic": null,
        "pic_t": null
    }
}
```


## 上传图片

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/orders/<OID>/order_pics"
```
### HTTP Request

    `POST http://amblem.xijitech.com/api/v1/orders/<OID>/order_pics`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
OID|12|true|订单ID

order_pic[pic]| |true|图片

> 返回内容

```json
{
    "status": "success",
    "task_id": 2
}
```


## 删除任务图纸

```shell
curl -X DELETE "http://amblem.xijitech.com/api/v1/orders/<TID>/order_pics/<PID>"
```
### HTTP Request

    `DELETE http://amblem.xijitech.com/api/v1/orders/<TID>/order_pics/<PID>`

### Query Parameters

Parameter | Description
--------- | -----------
TID|任务ID
PID|图纸ID

> 返回内容

```json
{
    "status": "success",
    "order_id": 10
}
```


## 获取订单类型

```shell
curl "http://amblem.xijitech.com/api/v1/orders/fields_from_type?order_type_id=<ID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/orders/fields_from_type?order_type_id=<ID>`

### Query Parameters

Parameter | Description
--------- | -----------
ID|订单类型ID

> 返回内容

```json
{
    "type_name": "柜子",
    "fields_count": [
        {
            "id": 6,
            "name": "柜子面",
            "order_type_id": 4,
            "price": 0,
            "default_unit": "元",
            "created_at": "2017-12-06T11:08:55.546+08:00"
        }
    ],
    "fields": [
        {
            "id": 7,
            "name": "柜子",
            "order_type_id": 4,
            "price": 1000,
            "default_unit": "个",
            "created_at": "2017-12-06T11:08:55.549+08:00"
        },
        {
            "id": 8,
            "name": "柜子布",
            "order_type_id": 4,
            "price": 0,
            "default_unit": "",
            "created_at": "2017-12-06T15:29:33.967+08:00"
        }
    ]
}
```

## 订单进行下一步

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/orders/<OID>/operate_event"
```

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/orders/<OID>/operate_event`


### FormData

Parameter | Required | Description
--------- | ------- | ---------
OID|true|订单ID


> 返回内容

```json
{
  "status":"success"
}
```


## 任务退回上一步

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/orders/<OID>/reject_event" -d "reject_reason=REASON"
```

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/orders/<OID>/reject_event`

### FormData

Parameter | Required | Description
--------- | ------- | ---------
OID|true|订单ID
reject_reason|true|退回理由（任务进行下一步没有此项）


> 返回内容

```json
{
  "status":"success"
}
```



## 创建订单

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/orders"
```

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/orders`


### FormData

Parameter | Required | Description
--------- | ------- | ---------
order[serial_num_user]|fales|制造单号
order[order_type_id]|true|订单类型ID
order[task_id]|true|任务ID
order[express_cost]|true|运费
order[express_num]|false|物流单号
order[properties]|true|订单类型根据客户输入生成：{"台面":"112", "台面布":"112"}
order[remarks]|false|备注


> 返回内容

```json
{
    "status": "success",
    "order_id": 7
}
```




## 更新订单

```shell
curl -X PATCH "http://amblem.xijitech.com/api/v1/orders/<ID>"
```

### HTTP Request

	`PATCH http://amblem.xijitech.com/api/v1/orders/<ID>`


### FormData

Parameter | Required | Description
--------- | ------- | ---------
ID|true|订单ID
order[serial_num_user]|fales|制造单号
order[order_type_id]|true|订单类型ID
order[task_id]|true|任务ID
order[express_cost]|true|运费
order[express_num]|false|物流单号
order[properties]|true|订单类型根据客户输入生成：{"台面":"112", "台面布":"112"}
order[remarks]|false|备注
order[installer_id]|false|安装师傅ID



> 返回内容

```json
{
    "status": "success",
    "order_id": 2
}
```
