# 首页

## 获取商品分类

```shell
curl "http://amblem.xijitech.com/api/v1/seriies"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/seriies`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "messages": 5,
    "categories": [
        {
            "id": 5,
            "name": "厨柜产品",
            "created_at": "2017-10-05T10:29:19.407+08:00"
        }
    ],
    "seriies": [
        {
            "id": 39,
            "category_id": 8,
            "name": "油烟机",
            "cover": "/uploads/covered_obj/cover/395/thumb_%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20171020194052.png",
            "created_at": "2017-10-20T18:43:16.204+08:00"
        }
    ]
}
```

## 获取商品分类下的商品(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/seriie?seriie=<ID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/seriie?seriie=<ID>`

### Query Parameters

Parameter | Description
--------- | -----------
ID|商品二级分类ID

> 返回内容

```json
{
    "seriie": {
        "id": 39,
        "category_id": 8,
        "name": "油烟机",
        "cover": [
            "/uploads/covered_obj/cover/395/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20171020194052.png"
        ],
        "created_at": "2017-10-20T18:43:16.204+08:00"
    },
    "merchandises": [
        {
            "id": 156,
            "name": "CXW-200-8229",
            "desc": "油烟机",
            "cover": "/uploads/covered_obj/cover/405/thumb_%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20171020194123.png",
            "prices": [
                1
            ],
            "created_at": "2017-10-20T20:02:40.276+08:00"
        }
    ]
}
```



## 查看商品

```shell
curl "http://amblem.xijitech.com/api/v1/merchandises/<ID>"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/merchandises/<ID>`

### Query Parameters

Parameter | Description
--------- | -----------
ID|商品ID

> 返回内容

```json
{
    "merchandise": {
        "id": 2,
        "name": "标准地柜",
        "desc": "标准地柜标准地柜",
        "introduce_summary": "<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>a's'da是</p>\r\n<p>asdaaa&nbsp;&nbsp;</p>\r\n<p>啊啊啊啊啊</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>aaaaa</p>\r\n<p>a</p>\r\n<p>aaaaaa</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>",
        "introduce_parameter": "<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>阿诗丹顿撒，，按时，大，是的，按时，啊，</p>\r\n<p>sad撒多，按时打卡上课打上卡什么打马赛克mkasdm&nbsp;</p>\r\n<p>大厦门口打什么可玛咖是没开打</p>\r\n<p>大厦门口打马赛克妈妈卡上的吗</p>\r\n<p>大厦门口打马赛克的吗卡神秘大咖没事的</p>\r\n<p>阿世盗名卡什么打卡说明大卡司mka</p>\r\n<p>&nbsp;打马赛克的马卡什么卡什么都看上</p>\r\n<p>按什么看打马赛克的马卡什么da</p>\r\n<p>打算买点卡什么快递马克是的嘛</p>\r\n<p>大厦门口打s</p>\r\n</body>\r\n</html>",
        "cover_thumb": [
            "/uploads/covered_obj/cover/2/thumb_haixinghezhenzhubeikeshiliangbeijing_3937174.jpg",
            "/uploads/covered_obj/cover/3/thumb_haixinghezhenzhubeikeshiliangbeijing_3937174.jpg",
            "/uploads/covered_obj/cover/4/thumb_99d58PIC6vm_1024.jpg"
        ],
        "cover": [
            "/uploads/covered_obj/cover/2/haixinghezhenzhubeikeshiliangbeijing_3937174.jpg",
            "/uploads/covered_obj/cover/3/haixinghezhenzhubeikeshiliangbeijing_3937174.jpg",
            "/uploads/covered_obj/cover/4/99d58PIC6vm_1024.jpg"
        ],
        "prices": [
            "20.00"
        ],
        "created_at": "2017-11-09T11:40:35.020+08:00"
    },
    "properties": [
        {
            "id": 9,
            "merchandise_id": 2,
            "price": "20.00",
            "cost": "0.00",
            "stock": "10",
            "sku": {
                "单双面": "单面",
                "表面处理": "高光",
                "颜色": "MK7101天山雪莲",
                "门型": "A01 B01"
            },
            "created_at": "2017-11-09T11:41:40.218+08:00"
        }
    ]
}
```
