# 用户

## 使用手机号登录

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/sessions" -d "login=TELEPHONE&password=PASSWORD"
```

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
login|15000000000|true|用户名
password|12345678|true|密码
wx_openid| |微信登录未绑定时点击绑定已有手机号|微信openId

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/sessions`

> 返回内容

```json
{
    "status": "success",
    "current_user": {
        "id": 1,
        "authentication_token": "596646418bea032c674a:1",
        "email": "",
        "created_at": "2017-09-28T10:28:22.541+08:00",
        "updated_at": "2017-11-21T09:58:10.001+08:00",
        "avatar": {
            "url": null,
            "thumb": {
                "url": null
            }
        },
        "nick": "工号9527",
        "telephone": "15000000000",
        "name": "工号9527",
        "gender": "1",
        "id_card": "工号9527",
        "id_card_cover": {
            "url": "/uploads/user/id_card_cover/1/Screenshot_20171019-162413.png",
            "thumb": {
                "url": "/uploads/user/id_card_cover/1/thumb_Screenshot_20171019-162413.png"
            }
        },
        "bank_card": "工号9527",
        "bank_card_cover": {
            "url": "/uploads/user/bank_card_cover/1/-6a2259467330ab9.png",
            "thumb": {
                "url": "/uploads/user/bank_card_cover/1/thumb_-6a2259467330ab9.png"
            }
        },
        "role_id": "admin",
        "operation_center_id": 2,
        "province_id": null,
        "city_id": null,
        "area_id": null,
        "approved": true,
        "wx_openid": null,
        "wx_avatar": null
    },
    "token": "596646418bea032c674a:1"
}
```


## 用户注册

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/registrations" -d "sms_code=SMS&user[telephone]=TELEPHONE&user[password]=PASSWORD&user[password_confirmation]=PASSWORDCONFIRMATION"
```

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
sms_code|123456|true|短信验证码
user[telephone]|15100000000|true|手机号
user[password]|123123123|true|密码
user[password_confirmation]|123123123|true|确认密码
wx_openid| |微信登录未绑定时点击注册|微信openId
nick| |微信登录未绑定时点击注册|微信昵称
wx_avatar| |微信登录未绑定时点击注册|微信头像

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/registrations`

> 返回内容

```json
{
    "current_user": {
        ...
    },
    "token": "596646418bea032c674a:1"
}
```


## 修改密码前验证手机号

```shell
curl "http://amblem.xijitech.com/api/v1/find_user?telephone=TELEPHONE"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/find_user?telephone=TELEPHONE`

### Query Parameters

Parameter | Required | Description
--------- | -------- | -----------
telephone|true|电话号码


> 返回内容

```json
{
    "status": "success",
    "telephone": "15100000000"
}
```




## 发送短信验证码

```shell
curl "http://amblem.xijitech.com/api/v1/sms_code?mobile=TELEPHONE"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/sms_code?mobile=TELEPHONE`

### Query Parameters

Parameter | Required | Description
--------- | -------- | -----------
mobile|true|电话号码


> 返回内容

```json
{
    "status": "success"
}
```


## 修改密码

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/update_password" -d "sms_code=SMS&user[telephone]=TELEPHONE&user[password]=PASSWORD&user[password_confirmation]=PASSWORDCONFIRMATION"
```

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
sms_code|123456|true|短信验证码
user[telephone]|15100000000|true|手机号
user[password]|123123123|true|密码
user[password_confirmation]|123123123|true|确认密码

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/update_password`

> 返回内容

```json
{
    "status": "success"
}
```



## 微信登录

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/weixin_login" -d "code=CODE"
```

此处判断`wx_openid`是否存在，若存在返回用户数据，不存在，需将`wx_openid`传至 `用户登录` 或 `用户注册`，登录或注册成功后会更新该用户的`wx_openid`值

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
code| |true|code

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/weixin_login`

> 返回内容

```json
{
    "status": "failed",
    "msg": "该微信号未绑定手机，请注册或绑定手机",
    "wx_openid": "...",
    "nick": "...",
    "wx_avatar": "..."
}

{
    "status": "success",
    "current_user": {
        "id": 1,
        "authentication_token": "596646418bea032c674a:1",
        "email": "",
        "created_at": "2017-09-28T10:28:22.541+08:00",
        "updated_at": "2017-11-21T09:58:10.001+08:00",
        "avatar": {
            "url": null,
            "thumb": {
                "url": null
            }
        },
        "nick": "工号9527",
        "telephone": "15000000000",
        "name": "工号9527",
        "gender": "1",
        "id_card": "工号9527",
        "id_card_cover": {
            "url": "/uploads/user/id_card_cover/1/Screenshot_20171019-162413.png",
            "thumb": {
                "url": "/uploads/user/id_card_cover/1/thumb_Screenshot_20171019-162413.png"
            }
        },
        "bank_card": "工号9527",
        "bank_card_cover": {
            "url": "/uploads/user/bank_card_cover/1/-6a2259467330ab9.png",
            "thumb": {
                "url": "/uploads/user/bank_card_cover/1/thumb_-6a2259467330ab9.png"
            }
        },
        "role_id": "admin",
        "operation_center_id": 2,
        "province_id": null,
        "city_id": null,
        "area_id": null,
        "approved": true,
        "wx_openid": null,
        "wx_avatar": null
    },
    "token": "596646418bea032c674a:1"
}
```
