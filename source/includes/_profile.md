# 个人

## 用户中心

```shell
curl "http://amblem.xijitech.com/api/v1/profile"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/profile`

### Query Parameters


Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "avatar": null,
    "user_nick": "工号9527",
    "approved": "已激活"
}
```


## 账户设置

```shell
curl "http://amblem.xijitech.com/api/v1/setting"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/setting`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "user_id": 1,
    "user_telephone": "15000000000",
    "user_nick": "工号9527"
}
```


## 基本信息

```shell
curl "http://amblem.xijitech.com/api/v1/base_info"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/base_info`

### Query Parameters

Parameter | Description
--------- | -----------


> 返回内容

```json
{
    "user": {
        "id": 1,
        "avatar": null,
        "name": "工号9527",
        "nick": "工号9527",
        "approved": true,
        "operation_center_name": "上海-苏州运营中心",
        "stores": [
            "南桥贤马店"
        ],
        "role": "管理员",
        "telephone": "15000000000",
        "gender": "男",
        "province_id": 9,
        "city_id": 76,
        "area_id": 757,
        "province": "上海",
        "city": "上海",
        "area": "黄浦区",
        "id_card": "工号9527",
        "id_card_cover": {
            "url": "/uploads/user/id_card_cover/1/Screenshot_20171019-162413.png",
            "thumb": {
                "url": "/uploads/user/id_card_cover/1/thumb_Screenshot_20171019-162413.png"
            }
        },
        "bank_card": "工号9527",
        "bank_card_cover": {
            "url": "/uploads/user/bank_card_cover/1/-6a2259467330ab9.png",
            "thumb": {
                "url": "/uploads/user/bank_card_cover/1/thumb_-6a2259467330ab9.png"
            }
        }
    },
    "role": {
        "user": "基本用户",
        "admin": "管理员",
        "yunyingguanlizhe": "运营管理者",
        "yewu": "业务",
        "baojiacaiwu": "报价财务",
        "shejizhuguan": "设计主管",
        "shejishi": "设计师",
        "shouhouzhuguan": "售后主管",
        "anzhuangshifu": "安装师傅",
        "boss": "老板",
        "dianzhang": "店长",
        "xiaoshou": "销售",
        "disanfangqudao": "第三方渠道"
    },
    "operation_centers": [
        {
            "id": 2,
            "name": "上海-苏州运营中心"
        }
    ],
    "stores": [
        {
            "id": 7,
            "name": "南桥贤马店"
        }
    ],
    "province": [
        {
            "id": 9,
            "name": "上海"
        }
    ],
    "cities": [
        {
            "id": 76,
            "name": "上海"
        }
    ],
    "areas": [
        {
            "id": 776,
            "name": "其他"
        }
    ]
}
```


## 用户管理(分页page)

```shell
curl "http://amblem.xijitech.com/api/v1/user_management?approved=STATUS"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/user_management?approved=STATUS`

### Query Parameters


Parameter | Required | Description
--------- | -------- | -----------
approved|false|激活状态(true,false)



> 返回内容

```json
{
    "users": [
      {
          "id": 2,
          "nick": "安装师9527",
          "name": "1",
          "approved": true,
          "operation_center_name": "上海运营中心",
          "stores": [
              "上海三店"
          ],
          "role": "安装师傅",
          "telephone": "18621335061"
      },
      {
          "id": 5,
          "nick": null,
          "name": null,
          "approved": false,
          "operation_center_name": null,
          "stores": [],
          "role": "基本用户",
          "telephone": "15300000000"
      }
    ]
}
```


## 查看用户信息

```shell
curl "http://amblem.xijitech.com/api/v1/profile/<UID>/user_info"
```
### HTTP Request

    `GET http://amblem.xijitech.com/api/v1/profile/<UID>/user_info`

### Query Parameters


Parameter | Required | Description
--------- | -------- | -----------
UID|true|用户ID



> 返回内容

```json
{
    "user": {
        "id": 29,
        "avatar": "",
        "name": "杨志",
        "nick": "Arss杨",
        "approved": true,
        "operation_center_name": "上海-苏州运营中心",
        "stores": [
            "漕宝永乐店",
            "南桥贤马店",
            "宝山建配龙店",
            "徐汇波涛店",
            "苏州总店",
            "昆山港龙店"
        ],
        "role": "设计主管",
        "telephone": "15618658896",
        "gender": "男",
        "province_id": 9,
        "city_id": 76,
        "area_id": 757,
        "province": "上海",
        "city": "上海",
        "area": "黄浦区",
        "id_card": "",
        "id_card_cover": "未上传",
        "bank_card": "",
        "bank_card_cover": "未上传"
    }
}
```


## 修改用户信息（需要上传图片）

```shell
curl -X POST "http://amblem.xijitech.com/api/v1/update_info"
```

### HTTP Request

	`POST http://amblem.xijitech.com/api/v1/update_info`


### FormData

Parameter | Required | Description
--------- | ------- | ---------
user[avatar]|false|头像照片
user[nick]|false|昵称（系统里用的都是这个）
user[operation_center_id]|false|所属运营中心
user[store_ids][]|false|所属门店
user[role_id]|false|角色
user[name]|false|真实姓名
user[gender]|false|性别
user[province_id]|false|地区
user[city_id]|false|地区
user[area_id]|false|地区
user[id_card]|false|身份证号码
user[id_card_cover]|false|身份证照片
user[bank_card]|false|银行卡号
user[bank_card_cover]|false|银行卡照片


> 返回内容

```json
{
  "status":"success"
}
```



## 激活或封闭用户

```shell
curl -X PUT "http://amblem.xijitech.com/api/v1/profile/<UID>/approved"
```
### HTTP Request

    `PUT http://amblem.xijitech.com/api/v1/profile/<UID>/approved`

### FormData

Parameter | Default | Required | Description
--------- | ------- | ---------| -----------
UID|1|true|用户ID


> 返回内容

```json
{
    "status": "success"
}
```
