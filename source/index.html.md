---
title: API Reference

language_tabs:
  - shell
  - ruby
  - javascript
  - java

toc_footers:
  - <a href='http://http://amblem.xijitech.com/'>Amblem</a>
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - users
  - profile
  - address
  - tasks
  - orders
  - carts
  - offering_lists
  - messages
  - merchandises
  - abouts
  - sales_statistics
  - commissions
  - errors

search: true
---

# Introduction

Amblem API文档
